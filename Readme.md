# Projeto Texo

 O projeto foi feito com Java 8.

 Para rodar basta executar um "mvn install" e depois rodar o "com.prova.ProvaApplication".
 Uma vez que esteja rodando o serviço fica disponível em "http://localhost:8080/prova/winners"

 O arquivo "movielist.csv" se encontra em "prova\src\main\resources", 
 lá também tem o "testlist.csv" usado para o teste.