package com.prova;

import com.prova.h2.H2Connector;
import com.prova.h2.H2Manager;
import com.prova.io.CsvReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.ResultSet;
import java.util.ArrayList;

@SpringBootTest
public class ProvaApplicationTests {

	@BeforeClass
	public static void setUp() {
		H2Manager.createTable(H2Connector.getConnection());
		ArrayList<ArrayList<String>> table = CsvReader.getCsvAsTable("..\\main\\resources\\testlist.csv");
		H2Manager.populateTable(H2Connector.getConnection(), table);
	}

	@Test
	public void testRegisterQuantity() throws Exception {
		ResultSet rs = H2Manager.simpleSelect(H2Connector.getConnection(), "SELECT TITLE, COUNT(PRODUCERS) QTDREG FROM MOVIELIST GROUP BY TITLE");
		System.out.println("Testing line quantity");
		while (rs.next()) {
			switch (rs.getString("TITLE")) {
				case "Interstellar":
					Assert.assertEquals(3, rs.getInt("QTDREG"));
					break;
				case "Fight Club":
					Assert.assertEquals(3, rs.getInt("QTDREG"));
					break;
				case "Dummy 'ol Movie":
					Assert.assertEquals(1, rs.getInt("QTDREG"));
					break;
			}
		}
		System.out.println("Line quantity is fine.");
	}

	@Test
	public void testContent() throws Exception {
		System.out.println("Check if movie with title \"Dommy 'ol Movie\" has one producer called \" Regular Bob\".");
		ResultSet rs = H2Manager.simpleSelect(H2Connector.getConnection(), "SELECT COUNT(PRODUCERS) QTDREG FROM MOVIELIST WHERE TITLE = 'Dummy ''ol Movie' and PRODUCERS = ' Regular Bob'");
		rs.next();
		Assert.assertEquals(0, rs.getInt("QTDREG"));
		System.out.println("Checks out.");

		System.out.println("Check if movie with title \"Fight Club\" has one producer called \"Ceán Chaffin\".");
		rs = H2Manager.simpleSelect(H2Connector.getConnection(), "SELECT COUNT(PRODUCERS) QTDREG FROM MOVIELIST WHERE TITLE = 'Fight Club' AND PRODUCERS = 'Ceán Chaffin' AND YEAR = 1999");
		rs.next();
		Assert.assertEquals(1, rs.getInt("QTDREG"));
		System.out.println("Checks out.");

		System.out.println("Check if movie with title \"Interstellar\" has one producer called \"Christopher Nolan\" and one producer called \"Lynda Obst\".");
		rs = H2Manager.simpleSelect(H2Connector.getConnection(), "SELECT COUNT(PRODUCERS) QTDREG FROM MOVIELIST WHERE TITLE = 'Interstellar' AND PRODUCERS IN ('Christopher Nolan', 'Lynda Obst') AND YEAR = 2014");
		rs.next();
		Assert.assertEquals(2, rs.getInt("QTDREG"));
		System.out.println("Checks out.");
	}

}
