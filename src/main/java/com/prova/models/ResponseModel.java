package com.prova.models;

import lombok.Data;

import java.util.List;

@Data
public class ResponseModel {
    private List<WinnerModel> min;
    private List<WinnerModel> max;
}
