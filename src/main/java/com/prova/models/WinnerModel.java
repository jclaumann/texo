package com.prova.models;

import lombok.Data;

@Data
public class WinnerModel {
    private String producer;
    private int interval;
    private int previousWin;
    private int followingWin;
}
