package com.prova;

import com.prova.h2.H2Connector;
import com.prova.h2.H2Manager;
import com.prova.io.CsvReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ProvaApplication {

	public static void main(String[] args) {
		H2Manager.createTable(H2Connector.getConnection());

		ArrayList<ArrayList<String>> table = CsvReader.getCsvAsTable("..\\movielist.csv");

		H2Manager.populateTable(H2Connector.getConnection(), table);

		SpringApplication.run(ProvaApplication.class, args);
	}

}
