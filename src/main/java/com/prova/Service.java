package com.prova;

import com.prova.h2.H2Connector;
import com.prova.h2.H2Manager;
import com.prova.models.ResponseModel;
import com.prova.models.WinnerModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.ResultSet;
import java.util.ArrayList;

@RestController
@RequestMapping("/prova")
public class Service {

    @GetMapping(value = "/winners")
    public ResponseEntity<ResponseModel> test() {
        ResultSet rs = H2Manager.selectWinners(H2Connector.getConnection());
        if (rs == null) {
            System.out.println("The select returned null");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        ArrayList<WinnerModel> min = new ArrayList<WinnerModel>();
        ArrayList<WinnerModel> max = new ArrayList<WinnerModel>();
        try {
            while (rs.next()) {
                WinnerModel prod = new WinnerModel();
                prod.setProducer(rs.getString("PRODUCERS"));
                prod.setFollowingWin(rs.getInt("FOLLOWING_YEAR"));
                prod.setPreviousWin(rs.getInt("PREVIOUS_YEAR"));
                prod.setInterval(rs.getInt("YEAR_DIFF"));
                if (rs.getString("KIND").equalsIgnoreCase("MAX")) {
                    max.add(prod);
                } else {
                    min.add(prod);
                }
            }
            ResponseModel resp = new ResponseModel();
            resp.setMax(max);
            resp.setMin(min);
            return ResponseEntity.ok(resp);
        } catch (Throwable any) {
            any.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
