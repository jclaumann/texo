package com.prova.h2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

public class H2Manager {

    public static void createTable(Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("CREATE TABLE IF NOT EXISTS MOVIELIST (YEAR INT, TITLE VARCHAR(255), STUDIOS VARCHAR(255), PRODUCERS VARCHAR(255), WINNER BOOLEAN)");
        } catch (Throwable any) {
            any.printStackTrace();
        }
    }

    private enum Column {
        YEAR(0),
        TITLE(1),
        STUDIOS(2),
        PRODUCERS(3),
        WINNER(4);

        private int value;

        private Column (int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public static ResultSet simpleSelect(Connection conn, String select) {
        try {
            Statement sta = conn.createStatement();
            return sta.executeQuery(select);
        } catch (Throwable any) {
            System.out.println("An unexpected error as occurred. ErrorMsg:" + any.getMessage());
            any.printStackTrace();
            return null;
        }
    }

    public static ArrayList<String> getInsertBatch(ArrayList<ArrayList<String>> table) {
        ArrayList<String> batch = new ArrayList<String>();
        for (ArrayList<String> line : table) {
            if (line.equals(table.get(0))) {
                continue;
            }
            String insertMovie = "INSERT INTO MOVIELIST (YEAR, TITLE, STUDIOS, PRODUCERS, WINNER) VALUES (";
            ArrayList<String> producers = new ArrayList<String>();
            for (Column col: Column.values()) {
                switch (col) {
                    case YEAR:
                        insertMovie += line.get(col.getValue());
                        break;
                    case TITLE:
                    case STUDIOS:
                    case PRODUCERS:
                        String field = line.get(col.getValue());
                        field = field.replaceAll("'", "''");
                        if (col != Column.PRODUCERS) {
                            insertMovie += String.format(", '%s'", field);
                        } else {
                            insertMovie += ", '%s'";
                            producers.addAll(Arrays.asList(field.split(", and |, | and ")));
                        }
                        break;
                    case WINNER:
                        insertMovie += " ,";
                        //The CSV file does not have the last column if the movie didn't win
                        if (line.size() < 5) {
                            insertMovie += "'FALSE'";
                        } else {
                            insertMovie += (line.get(4).equalsIgnoreCase("yes")) ? "'TRUE'" : "'FALSE'";
                        }
                        break;
                }
            }

            insertMovie += ");";

            for (String producerName : producers) {
                batch.add(String.format(insertMovie, producerName.trim()));
            }
        }
        return batch;
    }

    public static void populateTable (Connection conn, ArrayList<ArrayList<String>> table) {
        try {
            ArrayList<String> batch = getInsertBatch(table);
            Statement st = conn.createStatement();
            for (String command: batch) {
                st.execute(command);
            }
        } catch (Throwable any) {
            any.printStackTrace();
        }
    }

    public static ResultSet selectWinners(Connection conn) {
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("WITH DATA AS (SELECT YEAR AS FOLLOWING_YEAR, " +
                                                                        "LAG(YEAR) OVER (PARTITION BY PRODUCERS ORDER BY PRODUCERS ASC, YEAR ASC) AS PREVIOUS_YEAR, " +
                                                                        "(YEAR - LAG(YEAR) OVER (PARTITION BY PRODUCERS ORDER BY PRODUCERS ASC, YEAR ASC)) AS YEAR_DIFF, PRODUCERS " +
                                                                 "FROM MOVIELIST " +
                                                                 "WHERE WINNER = TRUE AND " +
                                                                       "PRODUCERS IN (SELECT DISTINCT PRODUCERS FROM MOVIELIST WHERE WINNER = TRUE GROUP BY PRODUCERS HAVING COUNT(YEAR) > 1) " +
                                                                 "ORDER BY PRODUCERS ASC, YEAR ASC) " +
                                                  "SELECT 'MAX' AS KIND, FOLLOWING_YEAR, PREVIOUS_YEAR, YEAR_DIFF, PRODUCERS  FROM DATA WHERE YEAR_DIFF = (SELECT MAX(YEAR_DIFF) FROM DATA) " +
                                                  "UNION " +
                                                  "SELECT 'MIN' AS KIND,  FOLLOWING_YEAR, PREVIOUS_YEAR, YEAR_DIFF, PRODUCERS  FROM DATA WHERE YEAR_DIFF = (SELECT MIN(YEAR_DIFF) FROM DATA)");
            return result;
        } catch (Throwable any) {
            System.out.println("An unexpected error as occurred. ErrorMsg:" + any.getMessage());
            any.printStackTrace();
            return null;
        }
    }
}
