package com.prova.h2;

import java.sql.Connection;
import java.sql.DriverManager;

public class H2Connector {

    private static Connection conn;

    public static Connection getConnection() {
        if (conn == null) {
            createConnection();
        }
        return conn;
    }

    private static void createConnection() {
        try {
            conn = DriverManager.getConnection("jdbc:h2:mem:");
        } catch (Throwable any) {
            System.out.println("Unexpected error while creating conection. ErrorMsg: " + any.getMessage());
            any.printStackTrace();
        }
    }
}
