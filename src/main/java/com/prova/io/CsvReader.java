package com.prova.io;

import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;

public class CsvReader {

    @Value("${csv.path}")
    public String path;

    public static ArrayList<ArrayList<String>> getCsvAsTable(String filePath) {
        if (filePath != null && !filePath.trim().isEmpty() && filePath.endsWith("csv")) {
            File csvFile = new File(filePath);
            ArrayList<ArrayList<String>> table = getAsTable(csvFile);
            return table;
        } else {
            System.out.println("File path is empty or it is not a \"*.csv\".");
        }
        return null;
    }

    private static ArrayList<ArrayList<String>> getAsTable(File csvFile) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(csvFile));
            String line = reader.readLine();
            ArrayList<ArrayList<String>> tableLines = new ArrayList<ArrayList<String>>();
            while (line != null) {
                if (!line.trim().isEmpty()) {
                    tableLines.add(new ArrayList<String>(Arrays.asList(line.split(";"))));
                }
                line = reader.readLine();
            }
            return tableLines;
        } catch (Throwable any) {
            any.printStackTrace();
        }
        return null;
    }
}
